# Torque API
Listens to events from Torque App and publishes to vairous output plugins

[GitLab](https://gitlab.com/elad.bar/torque-api)

## How to use

### Run docker container using the following Docker Compose
```yaml
  torque-api:
    image: registry.gitlab.com/elad.bar/torque-api:latest
    restart: unless-stopped
    hostname: torque-api
    container_name: torque-api
    ports:
      - 8128:8128
    volumes:
      - ./config/config.json:/usr/src/app/config.json
      - ./data:/data
      - /etc/localtime:/etc/localtime:ro
      - /etc/timezone:/etc/timezone:ro

```

### Setup Tourqe App

In *Settings -> Data Logging & Upload*

Under the *Logging Preferences* header:

Touch *Select what to log*, activate the menu in the upper right, and select *Add PID to log*.
Select items of interest.

Under the *Realtime Web Upload* header:

Check *Upload to web-server*.
- Enter http://HOST:PORT/api/torque as the *Web-server URL*, where HOST and PORT are your externally accessible Torque API HTTP host and port. 
- Enter an email address in User Email Address (Same email address as in the `devices.yaml`)
- Note that it is recommanded to use HTTP with Torque App

### Configuration

Configuration file should be available at `/usr/src/app/config.json`, example:

```json
{
    "authentication": {
        "apiToken": "APITOKEN",
        "devices": {
            "email@email.com": "Name of the Car"
        }
    },
    "output": {
        "prometheus": {},
        "mqtt": {
            "host": "IP/Hostname",
            "port": 1883,
            "username": "username",
            "password": "password",
            "clientId": "TorqueAPIDEV",
            "topic": "torque/device-dev/status"
        },
        "memory": {
            "maximumInMemory": 10000,
            "flushInterval": 60,
            "outputDirectory": "/data"
        }
    }
}
```

#### Authentication Section
Represent configurations related to access the API, avoid configuring them will fail the load of the API 

##### API Token

```json
{
    "authentication": {
        "apiToken": "APITOKEN"
    }
}
```

##### Devices
Key-value pairs of email as configured in the TorqueApp and the desired device name in the output plugins

```json
{
    "authentication": {
        "devices": {
            "email@email.com": "Name of the Car"
        }
    }
}
```

#### Output Section
Represents the output plugins configuration, avoid configuring a specific output plugin will disable it

##### Prometheus

No need to defined anything but the output as in the example above

##### MQTT

Key | Type | Description | 
---|---|---|
host | string | hostname or IP of the MQTT Broker |
port | integer | port of the MQTT Broker
username | string | username
password | string | password
clientId | string | Client ID
topic | string | topic as will be published once event was sent

##### Memory

Key | Type | Description | 
---|---|---|
maximumInMemory | integer | Number of objects in memory
flushInterval | integer | Interval in second to flush to disk
outputDirectory | string | path to store the raw data and data

## Endpoints
API Token must be sent within the header 
```yaml
Authorization: Bearer {token}
```


Endpint | Method | Description | API Token | Depends on output configuration
---|---|---|---|---|
/ | GET | Readme | - | - |
/api/torque | GET | Report statistics (For Torque App) |  - | - |
/api/torque/raw | GET | Raw event's data | + | memory |
/api/torque/data | GET | Processed events | + | memory |
/api/torque/sensors | GET | List all available sensors |  + | - |
/api/debug | GET | Get debug mode | + | - |
/api/debug | POST | Set debug mode | + | - |
/api/debug | DELETE | Stop debug mode | + | - |