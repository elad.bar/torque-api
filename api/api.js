const express = require('express');
const slugify = require('slugify');
const fs = require('fs');
const md = require('markdown-it')();

const {ConsoleClient} = require("../clients/console.js");
const {MQTTClient} = require("../clients/mqtt.js");
const {MemoryClient} = require("../clients/memory.js");
const {PrometheusClient} = require("../clients/prometheus.js");
const {APIMiddleware} = require("./middlewares.js");

const CONSTS = require("./consts.js");

class TorqueAPI {
    constructor() {
        this.sensors = require(CONSTS.CONFIG_FILE_SENSORS);
        this.config = require(CONSTS.CONFIG_FILE); 
        this.api = express();
        this.middleware = new APIMiddleware();

        this.clients = [
            new ConsoleClient(),
            new MemoryClient(),
            new PrometheusClient(),
            new MQTTClient()
        ];

        this.readmeHtml = null;
    };

    Initialize() {
        console.debug("Initializing TorqueAPI");

        if (fs.existsSync(CONSTS.CONFIG_FILE_README_MD) && fs.existsSync(CONSTS.CONFIG_FILE_README_HTML)) {
            const readmeFileContent = fs.readFileSync(CONSTS.CONFIG_FILE_README_MD, CONSTS.README_FILE_ENCODING);
            const readmeContent = md.render(readmeFileContent);
    
            const readmeHTMLContent = fs.readFileSync(CONSTS.CONFIG_FILE_README_HTML, CONSTS.README_FILE_ENCODING);
            this.readmeHtml = readmeHTMLContent.replace(CONSTS.README_PLACEHOLDER, readmeContent)
        }     

        this.middleware.Initialize(this.config);

        if(this.middleware.enabled) {
            this.sensors.forEach(s => {
                s.name = slugify(s.description, CONSTS.SLUGIFY_CONFIG);
            });
    
            this.clients.forEach(c => c.Initialize(this.config));
    
            console.info(`Starting API on port ${CONSTS.API_PORT}`);
            
            this.bindEndpoints();
    
            this.api.listen(CONSTS.API_PORT);
        } else {
            console.error(`Cannot start API`);
        }        
    };

    findClient(clientName) {
        const clients = this.clients.filter(c => c.Name == clientName);

        return clients.length === 0 ? null : clients[0];
    };

    bindEndpoints() {
        const consoleClient = this.findClient("console");
        const memoryClient = this.findClient("memory");
        const prometheusClient = this.findClient("prometheus");
        
        const apiTokenCheck = this.middleware.apiTokenCheck.bind(this.middleware); 
        const userCheck = this.middleware.userCheck.bind(this.middleware);
        
        const devices = this.middleware.devices;
        const setResponseStatus = this.setResponseStatus;
        const convertToReadable = this.convertToReadable;
        const sensors = this.sensors;
        const readmeHtml = this.readmeHtml;

        this.api.get(CONSTS.ENDPOINT_DEBUG, apiTokenCheck, (req, res) => {
            setResponseStatus(res, 200, { debug: consoleClient.enabled });
        });
        
        this.api.post(CONSTS.ENDPOINT_DEBUG, apiTokenCheck, (req, res) => {
            consoleClient.Enable();
    
            setResponseStatus(res, 200, { debug: consoleClient.enabled });
        });
    
        this.api.delete(CONSTS.ENDPOINT_DEBUG, apiTokenCheck, (req, res) => {
            consoleClient.Disable();
    
            setResponseStatus(res, 200, { debug: consoleClient.enabled });
        });
    
        this.api.get(CONSTS.ENDPOINT_HOME, (req, res) => {
            setResponseStatus(res, 200, readmeHtml);
        });
        
        this.api.get(CONSTS.ENDPOINT_TORQUE_SENSORS, apiTokenCheck, (req, res) => {
            setResponseStatus(res, 200, sensors);
        });
    
        if(memoryClient.enabled) {
            this.api.get(CONSTS.ENDPOINT_TORQUE_RAW, apiTokenCheck, (req, res) => {
                setResponseStatus(res, 200, memoryClient.rawDataItems);
            });

            this.api.get(CONSTS.ENDPOINT_TORQUE_DATA, apiTokenCheck, (req, res) => {
                setResponseStatus(res, 200, memoryClient.dataItems);
            });
        }

        if(prometheusClient.enabled) {
            this.api.get(CONSTS.ENDPOINT_TORQUE_METRICS, apiTokenCheck, async (req, res) => {
                const content = await prometheusClient.GetMetrics();
                const contentType = prometheusClient.GetMetricsContentType();

                res.set('Content-Type', contentType);

                setResponseStatus(res, 200, content);
            });
        }

        this.api.get(CONSTS.ENDPOINT_TORQUE, [apiTokenCheck, userCheck], (req, res) => {
            this.clients.forEach(c => c.SendRaw(req.query));
    
            const data = convertToReadable(req.query, sensors, devices);
            
            this.clients.forEach(c => c.SendData(data));
    
            setResponseStatus(res, 200, CONSTS.TORQUE_STATS_RESPONSE_CONTENT);
        });
    };

    
    convertToReadable(data, sensors, devices) {
        const result = {};

        if(data !== null) {
            sensors.forEach(torqueSensor => {
                const key = torqueSensor.id;
                
                const tmpDataItem = data[key];

                if(tmpDataItem !== undefined) {
                    const isArray = torqueSensor.type === CONSTS.TYPE_ARRAY;
                    const isString = torqueSensor.type === CONSTS.TYPE_STRING;
                    
                    const dataItem = isArray ? tmpDataItem[0] : tmpDataItem;
                    
                    result[torqueSensor.name] = isString ? dataItem : parseFloat(dataItem);
                }
            });

            const username = result.username;
            result[CONSTS.TORQUE_STATS_DEVICE] = devices[username];
        }

        return result;
    };

    setResponseStatus(response, statusCode, content) {
        response.status(statusCode);

        if (content) {
            response.send(content);

            if(statusCode >= 400) {
                console.error(content);
            }
        }
    };
};

module.exports.TorqueAPI = TorqueAPI;