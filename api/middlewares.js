const fs = require('fs');
const CONSTS = require("./consts.js");

class APIMiddleware {
    constructor() {
        this.config = null;
        this.enabled = false;
    }

    get devices() {
        return this.config.authentication.devices;
    }

    get apiToken() { 
        return this.config.authentication.apiToken;
    }

    Initialize(config) {
        console.info("Initializing API Middleware");

        this.config = config;

        if(this.config && this.config.authentication) {
            const auth = this.config.authentication;

            const hasDevices = auth.devices && Object.keys(auth.devices).length > 0;
            const hasApiToken = auth.apiToken && auth.apiToken.length > 0;

            if(!hasDevices) {
                console.error("Devices mapping is not available in configuration");
            }

            if(!hasApiToken) {
                console.error("API Token is not available in configuration");
            }

            this.enabled = hasDevices && hasApiToken;

        } else {
            console.error("Invalid configuration");
        }
    }

    getEndpoint(route) {
        const endpoint = `${route}${CONSTS.API_KEY_PARAM}`;

        return endpoint;
    }

    userCheck(req, res, next){
        let httpCode = 401;
        let error = null;
        let errorLogMessage = null;

        const email = req.query.eml;

        if(email === undefined || email === null || email.length === 0) {
            error = `Invalid request data`;
            errorLogMessage = `${error}`;
            httpCode = 400;
        } else {
            const username = this.devices[email];

            if(!username) {
                error = `Failed to authenticate request to ${req.path}, Invalid EMail`;
                errorLogMessage = `${error} '${email}'`;
            }
        }        

        if(error === null) {
            
            return next();
        } else {
            console.error(errorLogMessage);
            res.status(httpCode).send({ error });
        }
    }

    apiTokenCheck(req, res, next) {
        const authorization = req.headers.authorization || req.headers.Authorization;
        const authParts = !!authorization ? authorization.split(" ") : [];
        const isValidToken = authParts.length === 2 && authParts[0] === "Bearer" && this.apiToken === authParts[1];
        
        if(!isValidToken) {
            const error = `Failed to authenticate request to ${req.path}, Invalid API Token`;

            console.error(`${error} '${authorization}'`);
            res.status(401).send({ error });

            return;
        }

        return next();
    };
};

module.exports.APIMiddleware = APIMiddleware;

