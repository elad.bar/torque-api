const {ClientBase} = require("./client.js");

class ConsoleClient extends ClientBase {
    constructor() {
        super();

        this.client = null;
        this.name = "console";
      };

    Connect() {
        if (this.enabled) {
            this.isConnected = true;   
        }
    };

    SendRaw(message) {
        if(this.isConnected && this.enabled) {
            console.info(`Raw: ${JSON.stringify(message)}`);
        }
    };

    SendData(message) {
        if(this.isConnected && this.enabled) {
            console.info(`Data: ${JSON.stringify(message)}`);
        }
    };
};

module.exports.ConsoleClient = ConsoleClient;