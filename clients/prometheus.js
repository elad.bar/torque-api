const Prometheus = require('prom-client');
const {ClientBase} = require("./client.js");
const {METRICS} = require("../api/consts.js");

const CONSTS = require("../api/consts.js");


class PrometheusClient extends ClientBase {
    constructor() {
        super();

        this.client = null;
        this.name = "prometheus";
        this.registry = null;
        this.metricsSettins = {};
        this.sensors = null;
        this.devices = null;
      };

    get IsConnected() {
        return this.isConnected;
    }

    get Enabled() {
        return this.enabled;
    }

    Connect() {
        try {
            if (this.enabled) {
                console.info(`Starting prometheus server, Endpoint: /metrics`);

                this.devices = this.globalConfig.authentication.devices;
                this.client = new Prometheus.Registry();
                this.sensors = {};

                const sensors = require(CONSTS.CONFIG_FILE_SENSORS);

                sensors.forEach(sensor => {
                    this.sensors[sensor.id] = sensor;
                });
                
                METRICS.forEach(m => {
                    const gaugeSettings = {
                        name: m["name"], 
                        help: m["description"],
                        labelNames: m["labels"]
                    }

                    const gauge = new Prometheus.Gauge(gaugeSettings);

                    this.metricsSettins[m["name"]] = {
                        ...gaugeSettings,
                        "gauge": gauge
                    }

                    this.client.registerMetric(gauge);
                });               

                this.isConnected = true;                
            }
    
        } catch(ex) {
            console.error(`Failed to connect ${self.name}, Error: ${ex}`);
        }   
    };

    SendRaw(message) {
        if(this.isConnected && this.enabled){
            this.setReportMetric(message);
            this.setDeviceMetric(message);
        }
    };

    setReportMetric(message) {
        console.debug("Creating report");

        const metricSettings = this.metricsSettins["torque_report"];

        console.log(metricSettings)
        const gauge = metricSettings["gauge"];

        const username = message["eml"];
        const device = this.devices[username];

        const labels = {
            "device": device,
            "version": message["v"],
            "session": message["session"],
            "id": message["id"],
            "username": username
        };

        gauge.labels(labels).inc();

        console.debug("Report created");
    }

    setDeviceMetric(message) {
        console.debug("Report device metrics");

        const metricSettings = this.metricsSettins["torque_sensor"];
        const gauge = metricSettings["gauge"];

        const username = message["eml"];
        const device = this.devices[username];

        const ignoreKeys = ["eml", "v", "session", "id", "time"];

        const messageKeys = Object.keys(message);
        const metrics = messageKeys.filter(k => !ignoreKeys.includes(k));

        metrics.forEach(k => {
            const rawValue = message[k];
            const sensor = this.sensors[k];

            const description = this.getSensorSetting(sensor, "description", k);
            const unit = this.getSensorSetting(sensor, "unit", null);
            const minValue = this.getSensorSetting(sensor, "min", 0);
            const maxValue = this.getSensorSetting(sensor, "max", 0);
            
            const metricLabels = {
                "device": device,
                "key": k,
                "description": description,
                "unit": unit
            };
            
            const value = parseFloat(this.getMetricsValue(rawValue, minValue, maxValue));

            gauge.labels(metricLabels).set(value);
        });

        console.debug("Device metrics reported");
    };

    getSensorSetting(sensor, key, defaultValue) {
        if (!!sensor && !!sensor[key]) {
            return sensor[key];
        }

        return defaultValue;
    }

    getMetricsValue(rawValue, minValue, maxValue) {
        if(Array.isArray(rawValue)) {
            return rawValue.length > 0 ? rawValue[0] : minValue;
        } else if (rawValue === "Infinity") {
            return maxValue;
        }

        return rawValue;
    }

    async GetMetrics() {
        return await this.client.metrics();
    };

    GetMetricsContentType() {
        return this.client.contentType;
    }
};

module.exports.PrometheusClient = PrometheusClient;